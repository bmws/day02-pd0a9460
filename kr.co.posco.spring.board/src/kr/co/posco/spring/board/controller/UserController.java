package kr.co.posco.spring.board.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("user")
public class UserController {

	@RequestMapping("login.do")
	public String login(HttpSession session, String pass) {
		if("1234".equals(pass) ) {
			session.setAttribute("id", pass);
			session.setAttribute("name", pass);
		}
		
		return "redirect:/board/list.do";
	}
}
