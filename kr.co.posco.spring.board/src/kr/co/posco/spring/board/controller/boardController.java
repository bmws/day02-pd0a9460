
package kr.co.posco.spring.board.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.posco.spring.board.entity.Board;
import kr.co.posco.spring.board.service.BoardService;

@Controller
@RequestMapping("board")
public class boardController {

	private BoardService service;
	
	/*@RequestMapping("list.do")
	public String main() {
		return "redirect:list";
	}*/
	
	@RequestMapping("list.do")
	public String findAllBoards(Model model) {
		List<Board> boards = service.findAllBoards();
		//ModelAndView mav = new ModelAndView("list");
		//mav.addObject("board", boardList);
		model.addAttribute("boards",boards);
		return "article/articleMain";
	}
	
	
	

	/*@RequestMapping("/searchByName")
	public String searchByName(@RequestParam("musicName") String name, Model model) {
		List<Board> musicList = service.findByName(name);
		model.addAttribute(musicList);
		return "list";
	}*/
	
	
}
