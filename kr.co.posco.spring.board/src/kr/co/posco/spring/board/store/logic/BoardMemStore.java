package kr.co.posco.spring.board.store.logic;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.co.posco.spring.board.entity.Board;
import kr.co.posco.spring.board.store.BoardStore;

@Repository
public class BoardMemStore implements BoardStore {

	private BoardRepository repo;

	public BoardMemStore() {
		//
		this.repo = BoardRepository.getInstance();
	}

	public String create(Board board) {
		//
		return repo.insertBoard(board);
	}

	public Board retrieve(String boardId) {
		//
		return repo.selectBoard(boardId);
	}

	public void update(Board board) {
		//
		repo.updateBoard(board);
	}

	public void delete(String boardId) {
		//
		repo.deleteBoard(boardId);
	}

	public List<Board> retrieveAll() {
		//
		return repo.selectAllBoards();
	}
}
