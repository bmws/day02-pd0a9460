package kr.co.posco.spring.board.store;

import java.util.List;

import kr.co.posco.spring.board.entity.Comment;

public interface CommentStore {

	String create(Comment comment);

	List<Comment> retrieveAll(String articleId);

	void delete(String commentId);
}
