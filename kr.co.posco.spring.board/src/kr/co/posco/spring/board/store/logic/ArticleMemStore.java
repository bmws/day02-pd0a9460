package kr.co.posco.spring.board.store.logic;

import java.util.List;

import kr.co.posco.spring.board.entity.Article;

public class ArticleMemStore {

    private BoardRepository repo;
    
    public ArticleMemStore() {
        //
        this.repo = BoardRepository.getInstance();
    }
    
    public String create(Article article) {
        //
        return repo.insertArticle(article);
    }

    public Article retrieve(String articleId) {
        // 
        return repo.selectArticle(articleId);
    }

    public List<Article> retrieveAll(String boardId) {
        // 
        return repo.selectAllArticle(boardId);
    }

    public void update(Article article) {
        // 
        repo.updateArticle(article);
    }

    public void delete(String articleId) {
        //
        repo.deleteArticle(articleId);
    }

}
