package kr.co.posco.spring.board.store;

import java.util.List;

import kr.co.posco.spring.board.entity.Board;

public interface BoardStore {

    String create(Board board);
    Board retrieve(String boardId);
    void update(Board board);
    void delete(String boardId);
	List<Board> retrieveAll();
}
