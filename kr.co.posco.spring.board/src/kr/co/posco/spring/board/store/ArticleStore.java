package kr.co.posco.spring.board.store;

import java.util.List;

import kr.co.posco.spring.board.entity.Article;

public interface ArticleStore {
    
    String create(Article article);
    Article retrieve(String articleId);
    List<Article> retrieveAll(String boardId);
    void update(Article article);
    void delete(String articleId);
}
