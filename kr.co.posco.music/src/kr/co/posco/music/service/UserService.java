package kr.co.posco.music.service;

import kr.co.posco.music.domain.User;

public interface UserService {
	//
	User login(User user);

	boolean register(User user);

	User find(String loginId);
}
