package kr.co.posco.music.service;

import java.util.List;

import kr.co.posco.music.domain.Music;

public interface MusicService {
	//
	Music find(int id);

	List<Music> findByName(String name);

	List<Music> findAll();
}
