package kr.co.posco.music.service.logic;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.co.posco.music.domain.Music;
import kr.co.posco.music.store.MusicStore;
import kr.co.posco.music.store.repository.MusicPlayRepository;

@Repository
public class MugicMemStore implements MusicStore{

	private MusicPlayRepository repo;
	
	public MugicMemStore() {
		this.repo = MusicPlayRepository.createInstance();
	}
	
	public Music read(int id) {
		// TODO Auto-generated method stub
		return repo.readMusic(id);
	}

	public List<Music> readByName(String name) {
		// TODO Auto-generated method stub
		return repo.readMusicByName(name);
	}

	public List<Music> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
