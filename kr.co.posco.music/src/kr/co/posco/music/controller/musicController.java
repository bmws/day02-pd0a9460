
package kr.co.posco.music.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.posco.music.domain.Music;
import kr.co.posco.music.service.MusicService;

@Controller
public class musicController {

	private MusicService service;
	
	@RequestMapping("/")
	public String main() {
		return "redirect:list";
	}
	
	@RequestMapping("/list")
	public ModelAndView showAllMusic() {
		List<Music> musicList = service.findAll();
		ModelAndView mav = new ModelAndView("list");
		mav.addObject("music", musicList);
		return mav;
	}

	@RequestMapping("/searchByName")
	public String searchByName(@RequestParam("musicName") String name, Model model) {
		List<Music> musicList = service.findByName(name);
		model.addAttribute(musicList);
		return "list";
	}
	
	
}
