package kr.co.posco.music.store;

import kr.co.posco.music.domain.User;

public interface UserStore {
	//
	boolean create(User user);

	User read(String id);
}
