package kr.co.posco.music.store;

import java.util.List;

import kr.co.posco.music.domain.Music;

public interface MusicStore {
	//
	Music read(int id);

	List<Music> readByName(String name);

	List<Music> readAll();
}
