package kr.co.posco.music.store;

import java.util.List;

import kr.co.posco.music.domain.Music;

public interface UserMusicStore {
	//
	boolean create(String userId, int musicId);

	boolean delete(String userId, int musicId);

	boolean existUserMusic(String userId, int musicId);

	List<Music> readMusicsByUser(String userId);
}
